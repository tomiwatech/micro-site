$(document).ready(function () {

    "use strict";
     // Hide the download QR functionality
     $("#qr").hide();

    // Run this function on the background to call acount name Endpoint
    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 5000;  //time in ms, 5 second for example
    var $input = $('#accountNumber');

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(fetchAccountName, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function fetchAccountName() {
        var accountnumber = document.getElementById('accountNumber').value;

        var bankID = document.getElementById('bankName');
        var bankCode = bankID.options[bankID.selectedIndex].value;
        var accountnumber = document.getElementById('accountNumber').value;

        var data = {
            bankCode: bankCode,
            accountNumber: accountnumber
        }

        // call api to fecth bankname
        $.ajax({
            url: 'http://178.62.86.202:1994/api/getBankDetails',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function (res) {
                // alert(JSON.stringify(res));
                var acctName = res.object['accountName'];
                document.getElementById("accountName").value = acctName;
                console.log(res);
            },
            error: function (res) {
                // alert(JSON.stringify(res));
                console.log(res);
            }
        });
    }

    //

    var bankID = document.getElementById('bankName');
    var bankCode = bankID.options[bankID.selectedIndex].value;
    var accountnumber = document.getElementById('accountNumber').value;

    document.getElementById('myForm').addEventListener('submit', fetchCode);

    function fetchCode(e) {

        var firstname = document.getElementById('firstName').value;
        var lastname = document.getElementById('lastName').value;
        var phonenumber = document.getElementById('phoneNumber').value;
        var email = document.getElementById('emailId').value;
        var address = document.getElementById('Address').value;
        var businessname = document.getElementById('businessName').value;
        var bankname = document.getElementById('bankName').value;
        var accountname = document.getElementById('accountName').value;
        var accountnumber = document.getElementById('accountNumber').value;
        var middlename = document.getElementById('middleName').value;

        var data = {
            firstname: firstname,
            lastname: lastname,
            middlename: middlename,
            phone: phonenumber,
            email: email,
            address: address,
            business: businessname,
            account: accountnumber
        };

        if (!firstname || !lastname || !phonenumber || !email || !address || !businessname || !bankname || !accountnumber) {
            alert("PLEASE FILL ALL FIELDS");
        } else {
            $.ajax({
                url: 'http://178.62.86.202:1994/api/getQR',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                success: function (res) {
                    alert(JSON.stringify(res));
                    var uniqueId = res.object['uniqueId'];
                    var uniqueCode = res.object['uniqueCode'];
                    var qrcode = res.object['qrcode'];
                    
                    var imgSrc = 'data:image/png;base64,' + qrcode;
                    var datum = {
                        firstname: firstname,
                        lastname: lastname,
                        middlename: middlename,
                        phone: phonenumber,
                        email: email,
                        address: address,
                        business: businessname,
                        account: accountnumber,
                        uniqueId: uniqueId,
                        uniqueCode: uniqueCode,
                        qrcode: imgSrc
                    }
                    alert("Data to be sent to mongo " + JSON.stringify(datum));
                    // POST TO MONGO DATABASE FROM HERE
                    $.ajax({
                        url: '/users',
                        type: 'POST',
                        data: JSON.stringify(datum),
                        contentType: 'application/json',
                        success: function (res) {
                            alert(JSON.stringify(res));
                        },
                        error: function (data) {
                            alert(JSON.stringify(data));
                        }
                    });
                    // END OF POST TO MONGO DATABASE
                    
                    console.log(qrcode);
                    var input = document.getElementById('imagesrc');
                    input.src = imgSrc;
                    document.getElementById("qrcode").href = imgSrc ;
                    $("#myModal").modal("toggle");
                    $("#qr").show();

                   refreshPage();

                },
                error: function (data) {
                    console.log(data)
                    // alert(JSON.stringify(data));
                }
            });

       
        }
        e.preventDefault();
    }

    // empty all fields

    function refreshPage() {
        document.getElementById('firstName').value = "";
        document.getElementById('lastName').value = "";
        document.getElementById('phoneNumber').value = "";
        document.getElementById('emailId').value = "";
        document.getElementById('Address').value = "";
        document.getElementById('businessName').value = "";
        document.getElementById('bankName').value = "";
        document.getElementById('accountName').value = "";
        document.getElementById('accountNumber').value = "";
        document.getElementById('middleName').value = "";
    }

    function downloadQR(){
        
    }


})


