var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
        firstname : String,
        lastname : String,
        middlename : String,
        bank : String,
        accountname: String,
        business : String,
        email : String,
        phone : Number,
        uniqueCode : String,
        uniqueId: String,
        qrcode : String
    },
    {
        timestamps : {createdAt : 'created', updatedAt : 'updated'}
    });

var User = mongoose.model('User', UserSchema);

module.exports = {
    'User' : User
}