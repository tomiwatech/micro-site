var express = require('express');
var UserService = require('../services/user-service');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {

  UserService.allUsers(function (err, Users) {
    if (err) {
      return res.json({
        'responseCode': '03',
        'responseMessage': 'Error fetching Users'
      });
    }

    if (Users) {
      return res.json({
        'responseCode': '00',
        'responseMessage': 'Successfully fetched Users',
        'Users': Users
      });
    }

    return res.json({
      'responseCode': '02',
      'responseMessage': 'No Users in db'
    });
  });
});

//Find One
router.get('/:id', function (req, res, next) {
  var id = req.params.id;
  console.log(id);
  UserService.findUser(id, function (err, User) {
    if (err) {
      return res.json({
        'responseCode': '03',
        'responseMessage': 'Error fetching user'
      });
    }
    if (User) {
      return res.json({
        'responseCode': '00',
        'responseMessage': 'Successfully fetched users',
        'User': User
      });
    } return res.json({
      'responseCode': '02',
      'responseMessage': 'No users in db'
    });
  });
});

/* POST adds an new user. */
router.post('/', function (req, res, next) {
  var User = req.body;
  console.log(User.firstname);
  console.log(JSON.stringify(User));
  UserService.addUser(User, function (err, Users) {
    if (err) {
      return res.json({
        'responseCode': '03',
        'responseMessage': 'Error adding User'
      });
    }

    if (Users) {
      return res.json({
        'responseCode': '00',
        'responseMessage': 'Successfully added a User'
      });
    }

    return res.json({
      'responseCode': '02',
      'responseMessage': 'User exists already'
    });
  });
})


/* POST updates a user's record. */
router.post('/update', function (req, res, next) {
  var User = req.body;
  console.log(User.UserName);
  UserService.updateUser(User, function (err, Users) {
    if (err) {
      return res.json({
        'responseCode': '03',
        'responseMessage': 'Error adding user'
      });
    }

    if (Users) {
      return res.json({
        'responseCode': '00',
        'responseMessage': 'Successfully added user'
      });
    }

    return res.json({
      'responseCode': '02',
      'responseMessage': 'User exists already'
    });
  });
})


/* POST deletes a user's record. */
router.delete('/:id', function (req, res, next) {
  var id = req.params.id;
  console.log(id);
  UserService.deleteUser(id, function (err) {
    if (err) {
      return res.json({
        'responseCode': '03',
        'responseMessage': 'Error deleting User'
      });
    }

    return res.json({
      'responseCode': '00',
      'responseMessage': 'Successfully deleted User'
    });
  });
})

module.exports = router;
