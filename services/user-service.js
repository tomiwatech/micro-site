var User = require('../model/user').User;
var mysql = require('mysql');

var UserService = {};

// // connect to mysql database
// var db = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : 'programming',
//   database : 'microsite'
// });

// db.connect(function(err){
//     if(err){
//       console.log(err);
//     }else{
//       console.log("Successfully connected to mysql");
//     }
// });


UserService.findUser = function(account, next){

    User.findOne({"account" : account}, function(err, user){
        return next(err, user);
    });
}

UserService.addUser = function(data, next){
    this.findUser(data.account, function(err, user){
        if(err){
            console.log('Encountered error when searching if the User is in the db already');
            return next(err, null);
        }
        if(user){
            console.log('User with Account Number ' + user.account + ' exists already.');
            return next(null, null);
        }
        else{
            /*Add user to db*/
            var newUser = new User({
                firstname: data.firstname,
                lastname: data.lastname,
                middlename: data.middlename,
                email : data.email,
                phone: data.phone,
                account: data.account,
                business: data.business,
                uniqueCode : data.uniqueCode,
                uniqueId: data.uniqueId,
                qrcode: data.qrcode
            });

            newUser.save(function(err, user){
                return next(err, user);
            })
        }
    })
}

UserService.allUsers = function(next){
    User.find(function(err, users){
        return next(err, users);
    });
}

UserService.updateUser = function(userUser, next){

    User.update({"_id" : userUser._id}, {$set :
     {
        "UserName" : userUser.UserName,
        "UserPriority": userUser.UserPriority,
        "UserStatus": userUser.UserStatus,
        "UserDescription": userUser.UserDescription
    } 
       }, function(err, User){
        return next(err, User);
    })
}


// delete User with User._id
UserService.deleteUser = function (id, next) {

    User.remove({"_id" : id}, function (err) {
        return next(err);
    });
}

module.exports = UserService;